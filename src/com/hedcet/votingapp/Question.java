package com.hedcet.votingapp;

import android.R.string;

public class Question {
	int _id;
	String question;
	String category;
	String choice1;
	String choice2;
	int choice1_votes;
	int choice2_votes;
	public Question(){
		
	}
	public Question(int ID,String question,String category,String choice1,String choice2){
		this._id=ID;
		this.question=question;
		this.category=category;
		this.choice1=choice1;
		this.choice2=choice2;
		this.choice1_votes=0;
		this.choice2_votes=0;
	}
	public Question(int ID,String question,String category,String choice1,String choice2,int ch1_votes,int ch2_votes){
		this._id=ID;
		this.question=question;
		this.category=category;
		this.choice1=choice1;
		this.choice2=choice2;
		this.choice1_votes=ch1_votes;
		this.choice2_votes=ch2_votes;
	}
	public String getVotes(){
		return new String(Integer.toString(choice1_votes)+"-"+Integer.toString(choice2_votes));
	}
	public void setVotes(int v1,int v2){
		this.choice1_votes=v1;
		this.choice2_votes=v2;
	}
	public String getCategory(){
		return this.category;
	}
	public int getID(){
		return this._id;
	}
	public void setID(int id){
		this._id=id;
	}
	public String getQuestion(){
		return this.question;
	}
	public void setQuestion(String qstn){
		this.question = qstn;
	}
	public String getChoices(){
		return new String(this.choice1+"-"+this.choice2);
	}
	public void setChoices(String ch1,String ch2){
		this.choice1=ch1;
		this.choice2=ch2;
	}
	public void vote(int ch){
		if(ch==1)
			this.choice1_votes++;
		if(ch==2)
			this.choice2_votes++;
	}
}
