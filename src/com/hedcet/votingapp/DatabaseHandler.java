package com.hedcet.votingapp;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper{

	// All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;
 
    // Database Name
    private static final String DATABASE_NAME = "questionsManager";
 
    // Contacts table name
    private static final String TABLE_QUESTIONS = "questions";
 
    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_QUESTION = "question";
    private static final String KEY_CATEGORY = "category";
    private static final String KEY_CHOICE1 = "choice_one";
    private static final String KEY_CHOICE2 = "choice_two";
    private static final String KEY_CHOICE1_VOTE = "choice_one_votes";
    private static final String KEY_CHOICE2_VOTE = "choice_two_votes";
    
	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		String CREATE_QUESTIONS_TABLE = "CREATE TABLE " + TABLE_QUESTIONS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_QUESTION + " TEXT,"
                + KEY_CATEGORY + " TEXT," + KEY_CHOICE1 + " TEXT," + KEY_CHOICE2 + " TEXT," + KEY_CHOICE1_VOTE + " INTEGER," + KEY_CHOICE2_VOTE + " INTEGER" + ")";
        db.execSQL(CREATE_QUESTIONS_TABLE);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
		// TODO Auto-generated method stub
		 db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUESTIONS);
		 
	        // Create tables again
	        onCreate(db);
	}
	public void addQuestion(Question q1){
		SQLiteDatabase db = this.getWritableDatabase();
		
		String ch = new String(q1.getChoices());
		String[] Choices = ch.split("-");
		String Votes[] = q1.getVotes().split("-");
		Log.d("Votes==="+Votes[1]+" =="+Votes[0], q1.getVotes());
		ContentValues val = new ContentValues();
		val.put(KEY_ID, q1.getID());
		val.put(KEY_QUESTION, q1.getQuestion());
		val.put(KEY_CATEGORY, q1.getCategory());
		val.put(KEY_CHOICE1, Choices[0]);
		val.put(KEY_CHOICE2, Choices[1]);
		val.put(KEY_CHOICE1_VOTE, Integer.parseInt(Votes[0]));
		val.put(KEY_CHOICE2_VOTE, Integer.parseInt(Votes[1]));
				
		db.insert(TABLE_QUESTIONS, null, val);
	}
	public Question getQuestion(int id) {
	    SQLiteDatabase db = this.getReadableDatabase();
	 
	    Cursor cursor = db.query(TABLE_QUESTIONS, new String[] { KEY_ID,
	            KEY_QUESTION, KEY_CATEGORY,KEY_CHOICE1,KEY_CHOICE2,KEY_CHOICE1_VOTE,KEY_CHOICE2_VOTE }, KEY_ID + "=?",
	            new String[] { String.valueOf(id) }, null, null, null, null);
	    if (cursor != null)
	        cursor.moveToFirst();
	 
	    Question question = new Question(Integer.parseInt(cursor.getString(0)),
	            cursor.getString(1), cursor.getString(2),cursor.getString(3),cursor.getString(4),Integer.parseInt(cursor.getString(5)),Integer.parseInt(cursor.getString(6)));
	    // return contact
	    return question;
	}
	public List<Question> getQuestions(String category){
	
		List<Question> qstnList = new ArrayList<Question>();
		SQLiteDatabase db = this.getReadableDatabase();
		
		 
	    Cursor cursor = db.query(TABLE_QUESTIONS, new String[] { KEY_ID,
	            KEY_QUESTION, KEY_CATEGORY,KEY_CHOICE1,KEY_CHOICE2,KEY_CHOICE1_VOTE,KEY_CHOICE2_VOTE }, KEY_CATEGORY + "=?",
	            new String[] { category }, null, null, null, null);
	    if (cursor.moveToFirst()) {
	        do {
	            Question qstn = new Question(Integer.parseInt(cursor.getString(0)),cursor.getString(1),cursor.getString(2),cursor.getString(3)
	            		,cursor.getString(4),Integer.parseInt(cursor.getString(5)),Integer.parseInt(cursor.getString(6)));
	            //qstn.setID();
	            //qstn.setQuestion(cursor.getString(1));
	            //qstn.(cursor.getString(2));
	            // Adding contact to list
	            qstnList.add(qstn);
	        } while (cursor.moveToNext());
	    }
	 
		return qstnList;
		
	}
	public int updateQuestion(Question qstn) {
	    SQLiteDatabase db = this.getWritableDatabase();
	    String[] Choices = qstn.getChoices().split("-");
	    String[] Votes = qstn.getVotes().split("-");
	    ContentValues values = new ContentValues();
	    values.put(KEY_QUESTION, qstn.getQuestion());
	    values.put(KEY_CATEGORY, qstn.getCategory());
	    values.put(KEY_CHOICE1, Choices[0]);
	    values.put(KEY_CHOICE2, Choices[1]);
	    values.put(KEY_CHOICE1_VOTE, Integer.parseInt(Votes[0]));
	    values.put(KEY_CHOICE2_VOTE, Integer.parseInt(Votes[1]));
	 
	    // updating row
	    return db.update(TABLE_QUESTIONS, values, KEY_ID + " = ?",
	            new String[] { String.valueOf(qstn.getID()) });
	}

}
