package com.hedcet.votingapp;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends FragmentActivity {

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

	}

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		//		DatabaseHandler database = new DatabaseHandler(this);
		//		Log.d("Inserting  ==", "First qstn");
		//		database.addQuestion(new Question(2, "Who is your favorite bollywood actor ?", "bollywood", "Sharukh khan", "Salman khan"));
		//		Question q1 = database.getQuestion(1);
		//		List<Question> questions = database.getQuestions("sports");
		//		Log.d(questions.get(0).getQuestion(),"sports question===");

		//		Log.d("Readed Question:", q1.getQuestion());
		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a DummySectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.
			Fragment fragment = new DummySectionFragment();
			Bundle args = new Bundle();
			args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, position + 1);
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			// Show 3 total pages.
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			case 2:
				return getString(R.string.title_section3).toUpperCase(l);
			}
			return null;
		}
	}

	/**
	 * A dummy fragment representing a section of the app, but that simply
	 * displays dummy text.
	 */
	public static class DummySectionFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		public static final String ARG_SECTION_NUMBER = "section_number";

		public DummySectionFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main_dummy,
					container, false);
			final TextView dummyTextView = (TextView) rootView
					.findViewById(R.id.section_label);
			final RadioButton radiobt1 = (RadioButton) rootView.findViewById(R.id.radioButton1);
			final RadioButton radiobt2 = (RadioButton) rootView.findViewById(R.id.radioButton2);
			final Button btVote = (Button) rootView.findViewById(R.id.button1);

			final DatabaseHandler database = new DatabaseHandler(rootView.getContext());

			if(getArguments().getInt(ARG_SECTION_NUMBER)==1){
				final List<Question> qstns = database.getQuestions("sports");
				String[] choices = qstns.get(0).getChoices().split("-");
				dummyTextView.setText(qstns.get(0).getQuestion());
				dummyTextView.setTextSize(30);
				radiobt1.setText(choices[0]);
				radiobt1.setTextSize(15);
				radiobt2.setText(choices[1]);
				radiobt2.setTextSize(15);
				btVote.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(radiobt1.isChecked()==true){
							qstns.get(0).vote(1);
						}
						if(radiobt2.isChecked()){
							qstns.get(0).vote(2);
						}
						if(!(radiobt1.isChecked()) && !(radiobt2.isChecked())){
							Toast.makeText(arg0.getContext(), "Please select an option..", Toast.LENGTH_LONG).show();
						}else{
							database.updateQuestion(qstns.get(0));
							radiobt1.setVisibility(android.view.View.INVISIBLE);
							radiobt2.setVisibility(android.view.View.INVISIBLE);
							btVote.setVisibility(android.view.View.INVISIBLE);
							Float percent = new Float((qstns.get(0).choice1_votes*100)/(qstns.get(0).choice1_votes+qstns.get(0).choice2_votes));
							dummyTextView.setText(radiobt1.getText()+" got "+percent+"% votes \n"+
									radiobt2.getText()+" got "+Math.abs(100-percent)+"% votes \n Total votes:"+new Integer((qstns.get(0).choice1_votes+qstns.get(0).choice2_votes)));
							//Toast.makeText(arg0.getContext(), qstns.get(0).choice1_votes+"="+qstns.get(0).choice2_votes, Toast.LENGTH_LONG).show();
						}
					}
				});
			}
			if(getArguments().getInt(ARG_SECTION_NUMBER)==2){
				final List<Question> qstns = database.getQuestions("bollywood");
				String[] choices = qstns.get(0).getChoices().split("-");
				dummyTextView.setText(qstns.get(0).getQuestion());
				dummyTextView.setTextSize(30);
				radiobt1.setText(choices[0]);
				radiobt1.setTextSize(15);
				radiobt2.setText(choices[1]);
				radiobt2.setTextSize(15);
				btVote.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(radiobt1.isChecked()==true){
							qstns.get(0).vote(1);
						}
						if(radiobt2.isChecked()){
							qstns.get(0).vote(2);
						}
						if(!(radiobt1.isChecked()) && !(radiobt2.isChecked())){
							Toast.makeText(arg0.getContext(), "Please submit your vote..", Toast.LENGTH_LONG).show();
						}else{
							database.updateQuestion(qstns.get(0));
							radiobt1.setVisibility(android.view.View.INVISIBLE);
							radiobt2.setVisibility(android.view.View.INVISIBLE);
							btVote.setVisibility(android.view.View.INVISIBLE);
							Float percent = new Float((qstns.get(0).choice1_votes*100)/(qstns.get(0).choice1_votes+qstns.get(0).choice2_votes));
							dummyTextView.setText(radiobt1.getText()+" got "+percent+"% votes \n"+
									radiobt2.getText()+" got "+Math.abs(100-percent)+"% votes \n Total votes:"+new Integer((qstns.get(0).choice1_votes+qstns.get(0).choice2_votes)));
							//Toast.makeText(arg0.getContext(), qstns.get(0).choice1_votes+"="+qstns.get(0).choice2_votes, Toast.LENGTH_LONG).show();
						}
					}
				});

			}
			if(getArguments().getInt(ARG_SECTION_NUMBER)==3){
				dummyTextView.setText("About the app text content come here..");
				radiobt1.setVisibility(android.view.View.INVISIBLE);
				radiobt2.setVisibility(android.view.View.INVISIBLE);
				btVote.setVisibility(android.view.View.INVISIBLE);
			}
			//dummyTextView.setText(Integer.toString());
			return rootView;
		}
	}

}
